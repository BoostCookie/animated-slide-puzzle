# Animated Slide Puzzle

This is written in JavaScript. Run
`python -m http.server`
to look at the result in your browser.

The script `single-file.py` outputs the single html file that directly contains the `styles.css` and the `scripts/main.js`.
This output can be embedded into google sites, like [here](https://sites.google.com/view/conquerslidetest/startseite).
