#!/usr/bin/env python3
import os

def main():
    script_path = os.path.dirname(os.path.realpath(__file__))
    html_lines = open(os.path.join(script_path, "index.html"), "r").readlines()
    css_file = open(os.path.join(script_path, "styles.css"), "r").read()
    js_file = open(os.path.join(script_path, "scripts", "main.js"), "r").read()

    for line in html_lines:
        if "styles.css" in line:
            print("<style>")
            print(css_file, end="")
            print("</style>")
        elif "main.js" in line:
            print("<script>")
            print(js_file, end="")
            print("</script>")
        else:
            print(line, end="")

if __name__ == "__main__":
    main()
